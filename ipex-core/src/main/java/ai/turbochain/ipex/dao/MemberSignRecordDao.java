package ai.turbochain.ipex.dao;

import ai.turbochain.ipex.dao.base.BaseDao;
import ai.turbochain.ipex.entity.MemberSignRecord;

/**
 * @author jack
 * @Description:
 * @date 2020/5/410:18
 */
public interface MemberSignRecordDao extends BaseDao<MemberSignRecord> {
}
